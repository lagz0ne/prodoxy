package com.lag.prodoxy.model;

import org.joda.time.DateTime;

import com.ning.http.client.ProxyServer;
import com.ning.http.client.ProxyServer.Protocol;

public class ExtendedProxyServer {

	public ExtendedProxyServer() {
	}

	public ExtendedProxyServer(String protocol, String host, int port) {
		this.protocol = protocol;
		this.host = host;
		this.port = port;
	}

	public ExtendedProxyServer(ProxyServer proxy) {
		this.protocol = proxy.getProtocol().toString();
		this.host = proxy.getHost();
		this.port = proxy.getPort();
	}

	private long id;
	private String protocol;
	private String host;
	private int port = 80;
	private String testedWith;
	private boolean working;
	private long responseTime;
	private DateTime updatedAt;

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public DateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(DateTime createdAt) {
		this.createdAt = createdAt;
	}

	private DateTime createdAt;

	public ProxyServer getProxyServer() {
		return new ProxyServer(Protocol.valueOf(protocol), host, port);
	}

	public String getTestedWith() {
		return testedWith;
	}

	public void setTestedWith(String testedWith) {
		this.testedWith = testedWith;
	}

	public boolean isWorking() {
		return working;
	}

	public void setWorking(boolean working) {
		this.working = working;
	}

	public long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}

	public DateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(DateTime updatedAt) {
		this.updatedAt = updatedAt;
	}
}
