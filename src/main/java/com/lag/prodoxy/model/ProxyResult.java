package com.lag.prodoxy.model;

import org.joda.time.DateTime;

public class ProxyResult {

	public ProxyResult() {
		this.proxy = new ExtendedProxyServer();
	}

	public ProxyResult(
			ExtendedProxyServer proxy,
			String testedWith,
			long responseTime,
			boolean isWorking,
			DateTime updatedAt) {
		this.proxy = proxy;
		this.testedWith = testedWith;
		this.responseTime = responseTime;
		this.isWorking = isWorking;
		this.updatedAt = updatedAt;
		this.host = proxy.getHost();
		this.port = proxy.getPort();
		this.protocol = proxy.getProtocol();
	}

	private ExtendedProxyServer proxy;
	private String testedWith;
	private long responseTime;
	private String host;
	private int port;
	private String protocol;

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		proxy.setHost(host);
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		proxy.setPort(port);
		this.port = port;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		proxy.setProtocol(protocol);
		this.protocol = protocol;
	}

	public ExtendedProxyServer getProxy() {
		return proxy;
	}

	public void setProxy(ExtendedProxyServer proxy) {
		this.proxy = proxy;
	}

	public String getTestedWith() {
		return testedWith;
	}

	public void setTestedWith(String testedWith) {
		this.testedWith = testedWith;
	}

	public long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}

	public boolean isWorking() {
		return isWorking;
	}

	public void setWorking(boolean isWorking) {
		this.isWorking = isWorking;
	}

	public DateTime getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(DateTime updatedAt) {
		this.updatedAt = updatedAt;
	}

	private boolean isWorking;
	private DateTime updatedAt;
}