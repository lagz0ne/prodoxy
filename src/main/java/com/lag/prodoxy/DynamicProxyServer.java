package com.lag.prodoxy;

import java.util.Set;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.eventbus.EventBus;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.lag.prodoxy.checker.Checker;
import com.lag.prodoxy.services.CheckingService;
import com.lag.prodoxy.services.PersistenceService;
import com.lag.prodoxy.source.Source;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.AsyncHttpClientConfig;
import com.ning.http.util.SslUtils;

public class DynamicProxyServer {

	private static AsyncHttpClient asyncHttpClient = null;
	private static EventBus eventBus = new EventBus();
	private static Logger logger = LoggerFactory.getLogger(DynamicProxyServer.class);

	public static EventBus getEventBus() {
		return eventBus;
	}

	/**
	 * Keep it simple at start
	 */
	public static AsyncHttpClient getAsyncHttpClient() {
		SSLContext anonymousSSL = null;
		try {
			anonymousSSL = SslUtils.getSSLContext();
		} catch (Exception e) {
			System.out.println("Failed to get SSL context");
		}

		AsyncHttpClientConfig.Builder builder = new AsyncHttpClientConfig.Builder()
			.setUserAgent(
				"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.116 Safari/537.36")
			.setSSLContext(anonymousSSL)
			.setCompressionEnabled(true)
			.setConnectionTimeoutInMs(15000)
			.setRequestTimeoutInMs(15000)
			.setMaximumConnectionsPerHost(9)
			.setFollowRedirects(true)
			.setExecutorService(
				Executors.newCachedThreadPool(new ThreadFactoryBuilder()
					.setDaemon(true)
					.setNameFormat("async-http-client-%s")
					.build()));

		if (asyncHttpClient == null) {
			asyncHttpClient = new AsyncHttpClient(builder.build());
		}

		return asyncHttpClient;
	}

	public DynamicProxyServer() {
	}

	public final void start() throws Exception {
		logger.info("Starting checker service");
		CheckingService service = new CheckingService();
		service.startAsync();
		logger.info("Checker service started");

		Set<? extends Checker> checkers = Checker.getAllPartnerCheckers();

		for (Checker checker : checkers) {
			checker.setupConditions();
			eventBus.register(checker);
		}

		eventBus.register(PersistenceService.getInstance());
		Source.downloadAll();
	}

	public static void main(String[] args) throws Exception {
		DynamicProxyServer instance = new DynamicProxyServer();
		instance.start();
	}

}
