package com.lag.prodoxy.services;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.AbstractScheduledService;
import com.lag.prodoxy.DynamicProxyServer;
import com.lag.prodoxy.model.ExtendedProxyServer;

public class CheckingService extends AbstractScheduledService {

	@Override
	protected void runOneIteration() throws Exception {
		List<ExtendedProxyServer> proxies = PersistenceService.getInstance().getAllWorkingProxies();
		for (ExtendedProxyServer proxy : proxies) {
			DynamicProxyServer.getEventBus().post(proxy);
		}
	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedDelaySchedule(0, 10, TimeUnit.MINUTES);
	}

}