package com.lag.prodoxy.services;

import java.util.concurrent.TimeUnit;

import com.google.common.util.concurrent.AbstractScheduledService;
import com.lag.prodoxy.source.Source;

public class DownloadingService extends AbstractScheduledService {

	@Override
	protected void runOneIteration() throws Exception {
		// Trigger all sources
		Source.downloadAll();
	}

	@Override
	protected Scheduler scheduler() {
		return Scheduler.newFixedDelaySchedule(0, 1, TimeUnit.DAYS);
	}

}