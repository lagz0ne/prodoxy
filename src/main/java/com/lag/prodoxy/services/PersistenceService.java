package com.lag.prodoxy.services;

import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sql2o.Query;
import org.sql2o.Sql2o;

import snaq.db.DBPoolDataSource;

import com.google.common.collect.Maps;
import com.google.common.eventbus.AllowConcurrentEvents;
import com.google.common.eventbus.Subscribe;
import com.lag.prodoxy.model.ExtendedProxyServer;
import com.lag.prodoxy.model.ProxyResult;

public class PersistenceService {
	private DataSource datasource = null;

	private PersistenceService() {
		DBPoolDataSource ds = new DBPoolDataSource();
		ds.setName("pool-ds");
		ds.setDescription("Pooling DataSource");
		ds.setDriverClassName("com.mysql.jdbc.Driver");
		ds.setUrl("jdbc:mysql://localhost:3306/prodoxy");
		ds.setUser("duc");
		ds.setPassword("admin");
		ds.setMinPool(5);
		ds.setMaxPool(100);
		ds.setMaxSize(100);
		datasource = ds;

		query = new Sql2o(datasource);

		Map<String, String> columnMaps = Maps.newHashMap();
		columnMaps.put("host_name", "host");
		columnMaps.put("port_number", "port");
		columnMaps.put("protocol", "protocol");
		columnMaps.put("checked_with", "testedWith");
		columnMaps.put("response_time", "responseTime");
		columnMaps.put("updated_at", "updatedAt");
		columnMaps.put("is_working", "working");
		query.setDefaultColumnMappings(columnMaps);
	}

	private static PersistenceService instance = new PersistenceService();

	public static PersistenceService getInstance() {
		return instance;
	}

	private Logger logger = LoggerFactory.getLogger(PersistenceService.class);
	private Sql2o query = null;

	@Subscribe
	@AllowConcurrentEvents
	public void saveProxyResult(ProxyResult result) {
		Query _query = null;
		if (getProxy(result) == null) {
			_query = query
				.createQuery("INSERT INTO proxy_results (host_name, port_number, protocol, checked_with, is_working, response_time, updated_at) VALUES(:host, :port, :protocol, :checked_with, :is_working, :response_time, :updated_at)");
			if (logger.isInfoEnabled()) {
				logger.info("Saving proxy {}:{}", result.getProxy().getHost(), result
					.getProxy()
					.getPort());
			}
		} else if (!result.isWorking()) {
			_query = query
				.createQuery("DELETE FROM proxy_results WHERE host_name = :host AND port_number = :port AND protocol = :protocol AND checked_with = :checked_with");
			if (logger.isInfoEnabled()) {
				logger.info("Proxy {}:{} no longer works", result.getProxy().getHost(), result
					.getProxy()
					.getPort());
			}
		} else {
			_query = query
				.createQuery("UPDATE proxy_results SET is_working = :is_working, response_time = :response_time, updated_at = :updated_at WHERE host_name = :host AND port_number = :port AND protocol = :protocol AND checked_with = :checked_with");
			if (logger.isInfoEnabled()) {
				logger.info("Updating {}:{} stats", result.getProxy().getHost(), result
					.getProxy()
					.getPort());
			}
		}
		_query
			.addParameter("host", result.getProxy().getHost())
			.addParameter("port", result.getProxy().getPort())
			.addParameter("protocol", result.getProxy().getProtocol())
			.addParameter("checked_with", result.getTestedWith())
			.addParameter("is_working", result.isWorking() ? 1 : 0)
			.addParameter("response_time", result.getResponseTime())
			.addParameter("updated_at", DateTime.now());

		try {
			_query.executeUpdate();
		} catch (Exception e) {
			logger.error("Got exception {}: {}", e.getMessage(), e.getCause());
		}
	}

	public ProxyResult getProxy(ProxyResult result) {
		ExtendedProxyServer server = result.getProxy();
		Query selectProxy = query
			.createQuery(
				"SELECT * FROM proxy_results WHERE host_name = :host AND port_number = :port AND protocol = :protocol AND checked_with = :checked_with")
			.addParameter("host", server.getHost())
			.addParameter("port", server.getPort())
			.addParameter("protocol", server.getProtocol())
			.addParameter("checked_with", result.getTestedWith());
		return selectProxy.executeAndFetchFirst(ProxyResult.class);
	}

	public List<ExtendedProxyServer> getAllWorkingProxies() {
		Query selectProxies = query.createQuery(
			"SELECT * FROM proxy_results WHERE is_working = :is_working").addParameter(
			"is_working",
			1);
		List<ExtendedProxyServer> workingProxies = selectProxies
			.executeAndFetch(ExtendedProxyServer.class);

		return workingProxies;
	}

}
