package com.lag.prodoxy.source;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lag.prodoxy.DynamicProxyServer;
import com.ning.http.client.Response;

public class FromSchoolOfPrivacy extends Source {

	private Logger logger = LoggerFactory.getLogger(FromSchoolOfPrivacy.class);

	private static final String URL = "http://proxies.schoolofprivacy.eu/rss";

	@Override
	public String getSourceId() {
		return "schoolofprivacy.eu";
	}

	@Override
	public void run() throws Exception {
		if (logger.isInfoEnabled()) {
			logger.info("Start getting proxies from school of privacy");
		}

		// Retrieve result
		Response retrieveFeed = DynamicProxyServer
			.getAsyncHttpClient()
			.prepareGet(URL)
			.execute()
			.get();

		Element root = Jsoup.parse(retrieveFeed.getResponseBody());
		Elements items = root.select("channel > item > description");
		for (Element element : items) {
			String extractedText = element.text();

			String[] proxies = StringUtils.substringsBetween(extractedText, "<br/>", "$");

			if (proxies == null) {
				System.out.println(extractedText);
				continue;
			}

			for (String proxy : proxies) {
				String[] splitted = proxy.split(":|@");
				if (StringUtils.containsIgnoreCase(splitted[2], "SOCKS")) {
					continue;
				}

				this.add(splitted[2].trim(), splitted[0], Integer.valueOf(splitted[1]));
				if (logger.isInfoEnabled()) {
					logger.info("Proxy {}:{} is sent to check", splitted[2], splitted[1]);
				}
			}
		}

		if (logger.isInfoEnabled()) {
			logger.info("Finished retrieving data from school of privacy");
		}
	}

}
