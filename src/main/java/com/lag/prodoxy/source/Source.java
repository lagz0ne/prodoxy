package com.lag.prodoxy.source;

import java.util.Set;

import com.google.common.collect.Sets;
import com.lag.prodoxy.DynamicProxyServer;
import com.lag.prodoxy.model.ExtendedProxyServer;

public abstract class Source {

	public abstract String getSourceId();

	public abstract void run() throws Exception;

	private static Set<Source> sources;

	public static final Set<Source> getAllSources(boolean forceRefresh) throws Exception {
		sources = Sets.newHashSet();

		sources.add(new FromSchoolOfPrivacy());

		return sources;
	}

	public final void add(ExtendedProxyServer server) {
		DynamicProxyServer.getEventBus().post(server);
	}

	public final void add(String protocol, String host, int port) {
		this.add(new ExtendedProxyServer(protocol, host, port));
	}

	public static final Set<Source> getAllSources() throws Exception {
		return getAllSources(false);
	}

	public static final void downloadAll() throws Exception {
		Set<Source> sources = getAllSources();
		for (Source source : sources) {
			source.run();
		}
	}

}
