package com.lag.prodoxy.checker;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.google.common.base.Function;

public class ForJetstar extends Checker {

	@Override
	public void setupConditions() {
		this.setTestingUrl("http://booknow.jetstar.com/Search.aspx?culture=en-CA");
		this.addBodyCheckingCondition(new Function<String, Boolean>() {

			public Boolean apply(String input) {
				Document root = Jsoup.parse(input);
				return StringUtils.equalsIgnoreCase(root.title(), "Book Cheap Flights | Jetstar");
			}
		});
	}

	@Override
	public String getId() {
		return "jetstar.com";
	}

}
