package com.lag.prodoxy.checker;

import java.util.List;
import java.util.Set;

import org.joda.time.DateTime;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.eventbus.Subscribe;
import com.lag.prodoxy.DynamicProxyServer;
import com.lag.prodoxy.model.ExtendedProxyServer;
import com.lag.prodoxy.model.ProxyResult;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.HttpResponseHeaders;
import com.ning.http.client.HttpResponseStatus;
import com.ning.http.client.Request;
import com.ning.http.client.RequestBuilder;
import com.ning.http.client.Response;

public abstract class Checker {

	private Request testingRequest = null;
	private List<Function<HttpResponseHeaders, Boolean>> headerCheckers = Lists.newArrayList();
	private List<Function<HttpResponseStatus, Boolean>> statusCheckers = Lists.newArrayList();
	private List<Function<String, Boolean>> bodyCheckers = Lists.newArrayList();

	public abstract void setupConditions();

	public abstract String getId();

	protected boolean isAllOfRequestsMustPass() {
		return true;
	}

	protected boolean isAllOfTestMustPass() {
		return true;
	}

	public void setTestingUrl(String url) {
		testingRequest = new RequestBuilder().setUrl(url).build();
	}

	public void setTestingRequest(Request request) {
		testingRequest = request;
	}

	public void addHeaderCheckingCondition(Function<HttpResponseHeaders, Boolean> function) {
		headerCheckers.add(function);
	}

	public void addStatusCheckingCondition(Function<HttpResponseStatus, Boolean> function) {
		statusCheckers.add(function);
	}

	public void addBodyCheckingCondition(Function<String, Boolean> function) {
		bodyCheckers.add(function);
	}

	@Subscribe
	public void checkProxy(final ExtendedProxyServer proxy) throws Exception {
		final long startTime = System.currentTimeMillis();

		DynamicProxyServer
			.getAsyncHttpClient()
			.prepareRequest(testingRequest)
			.setProxyServer(proxy.getProxyServer())
			.execute(new AsyncCompletionHandler<ProxyResult>() {

				boolean success = true;

				@Override
				public com.ning.http.client.AsyncHandler.STATE onStatusReceived(
						HttpResponseStatus status) throws Exception {

					for (Function<HttpResponseStatus, Boolean> function : statusCheckers) {
						boolean result = function.apply(status);
						if (!result) {
							success = false;
						}
					}

					return super.onStatusReceived(status);
				}

				@Override
				public com.ning.http.client.AsyncHandler.STATE onHeadersReceived(
						HttpResponseHeaders headers) throws Exception {
					for (Function<HttpResponseHeaders, Boolean> function : headerCheckers) {
						boolean result = function.apply(headers);
						if (!result) {
							success = false;
						}
					}

					return super.onHeadersReceived(headers);
				}

				@Override
				public void onThrowable(Throwable t) {
				}

				@Override
				public ProxyResult onCompleted(Response response) throws Exception {
					long endTime = System.currentTimeMillis();

					String content = response.getResponseBody();

					for (Function<String, Boolean> function : bodyCheckers) {
						boolean result = function.apply(content);
						if (!result) {
							success = false;
						}
					}

					if (success) {
						ProxyResult result = new ProxyResult(
																proxy,
																getId(),
																endTime - startTime,
																success,
																DateTime.now());
						add(result);
						return result;
					}
					return null;
				}
			});
	}

	private void add(ProxyResult result) {
		DynamicProxyServer.getEventBus().post(result);
	}

	private static Set<Checker> subclasses = null;

	public static final Set<Checker> getAllPartnerCheckers(boolean forceRefresh) throws Exception {
		subclasses = Sets.newHashSet();

		subclasses.add(new ForJetstar());
		subclasses.add(new ForLionAir());

		return subclasses;
	}

	public static final Set<? extends Checker> getAllPartnerCheckers() throws Exception {
		return getAllPartnerCheckers(false);
	}

}