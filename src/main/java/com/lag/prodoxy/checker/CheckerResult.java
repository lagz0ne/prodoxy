package com.lag.prodoxy.checker;

/**
 * Per proxyserver per partner result
 * 
 * @author admin
 * 
 */
public class CheckerResult {

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private long averageProcessingTime;
	private boolean isSuccess = false;

	public long getAverageProcessingTime() {
		return averageProcessingTime;
	}

	public void setAverageProcessingTime(long averageProcessingTime) {
		this.averageProcessingTime = averageProcessingTime;
	}

	public boolean isSuccess() {
		return isSuccess;
	}

	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}

	@Override
	public String toString() {
		return "Proxy is " +
				(isSuccess ? "working" : "not working") +
				", average processing time: " +
				averageProcessingTime;
	}

}
